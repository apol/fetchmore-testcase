#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "testfetchmoremodel.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<TestFetchMoreModel>("org.kde.testytest", 1, 0, "TestFetchMoreModel");

    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();
}

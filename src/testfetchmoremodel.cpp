#include "testfetchmoremodel.h"

QVariant TestFetchMoreModel::data(const QModelIndex& index, int role) const
{
    if (role == Qt::DisplayRole) {
        return QString::number(index.row());
    }
    return {};
}

int TestFetchMoreModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 1;
}

int TestFetchMoreModel::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : m_lines;
}

QModelIndex TestFetchMoreModel::parent(const QModelIndex& /*child*/) const
{
    return {};
}

void TestFetchMoreModel::fetchMore(const QModelIndex& parent)
{
    if (parent.isValid()) {
        return;
    }
    qDebug() << "fetch more!" << m_lines;
    beginInsertRows(parent, m_lines, m_lines);
    m_lines++;
    endInsertRows();
}

#ifndef TESTFETCHMOREMODEL_H
#define TESTFETCHMOREMODEL_H

#include <qabstractitemmodel.h>

/**
 * @todo write docs
 */
class TestFetchMoreModel : public QAbstractListModel
{
    Q_OBJECT

public:
    QVariant data(const QModelIndex& index, int role) const override;
    int columnCount(const QModelIndex& parent) const override;
    int rowCount(const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;

    bool canFetchMore(const QModelIndex & parent) const override { return true; }
    void fetchMore(const QModelIndex & parent) override;

private:
    int m_lines = 3;
};

#endif // TESTFETCHMOREMODEL_H

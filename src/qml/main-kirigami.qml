import QtQuick 2.0
import QtQuick.Window 2.0
import org.kde.testytest 1.0
import org.kde.kirigami as Kirigami

Window
{
    visible: true
    width: 300
    height: 240

    // pageStack.initialPage: Kirigami.ScrollablePage {
        ListView {
            anchors.fill: parent

            model: TestFetchMoreModel {}
            delegate: Text {
                height: 50
                text: model.display
            }
        }
    // }
}

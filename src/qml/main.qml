import QtQuick
import QtQuick.Window
import org.kde.testytest 1.0

Window
{
    visible: true
    width: 300
    height: 240

    ListView {
        anchors.fill: parent
        reuseItems: true

        model: TestFetchMoreModel {}
        delegate: Text {
            height: 50
            text: model.display
        }
    }
}
